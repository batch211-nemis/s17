console.log("Hello World");


//Functions

//Functions in Java script are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked

//Functions are mostly created to create complicated tasks to run several lines of code in succession

//They are also used to prevent repeating lines/blocks of codes that perform same task/function

//Function Declarations
	//(function statement) defines a function with specified parameters

	/*
		Syntax:

		function functionName() {
			code block (statement)
	
		}
	*/
//function keyword - used to define a JavaScript function
//functionNAme - the function name. Functions are named to be able to use later in the code.
//function block ({}) - the statements which comprise the body of the function. This is where the code is to be executed
//we can assign a variable to hold a function, but that will be expalined later

function printName () {
	console.log("My name is John.");

};

printName();

//semicolons are used to separate executable JavaScript statements

//Function Invocation
	//the code block and statements inside a function is not a immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called
	//it is common to used a term 'call a function' instead of "invoke a function"

		printName ();

		// declaredFunction(); //error //much like our variables, we cannot a function we have yet to define

	//Function Declarations vs Expressions

		//Function Declarations
			//a function can be created through function declarartion by using the function keyword and adding a function name
			//declared fucntions are not executed immediately. they are saved for later use, and will be executed laer, when they are invoked (call upon).

			function declaredFunction() {
				console.log("Hello World from declaredFunction()");
			}

			declaredFunction();

		//Function Expression
			//a function can also be stored in a variable.
			//anonymous function assigned to the varaiableFunction (no function name)

			//variableFunction(); //error because it is stored in a variable

			let variableFunction = function() {
				console.log("Hello Again");
			};

			variableFunction();

			//we can also create a function expression of a named function
			//however, to invoke the function expression, we invoke it by its varaibale name, not by its function name
			//function expressions are always invoked (called) using the varaibale name

			let funcExpression = function funcName() {
				console.log("Hello from the other side");
			};

			//funcName(); //error
			funcExpression();

			//you can reassign declared functions and function expressions to new anonymous functions

			declaredFunction = function(){
				console.log("updated declaredFunction");
			};

			declaredFunction();

			funcExpression = function funcName() {
				console.log("updated funcExpression");
			};

			funcExpression();
			
			//however we cannot reassign a function expression initialized with const.

			const constantFunc = function(){
				console.log("Initialized with const!");
			};
			constantFunc();

			/*constantFunc = function(){
				console.log("cannot be re-assigned!");
			};
			constantFunc();*/ //error

	//Function Scoping

	/*
		Scope is the accessibility (visibility) of variables within our program JS variables has 3 types of scope:
		1.local/block scope
		2.global scope
		3.function scope
	*/

		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

		let globalVar = "Mr. Worldwide";
			console.log(globalVar);
	
	//Function Scope

	/*
		JS has a function scope: Each function creates a new scope
		Variables defined inside a function are not accessible (visible) from outside the function
		Variables declared with var, let, and const are quites similar when declared inside a function
	*/

	function showNames() {

		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};
		showNames();

		//Prompt
		let samplePrompt = prompt('Enter your Name');

		console.log("Hello, " + samplePrompt);

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);

		function printWelcomeMessage() {
			let firstName = prompt("Enter your Firtsname");
			let lastName = prompt("Enter your Lastname");
			console.log("Hello," + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}
		printWelcomeMessage();
// 
		//function Naming conventions

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		};

		getCourses();
